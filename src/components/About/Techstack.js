import React from "react";
import { Col, Row } from "react-bootstrap";
import { DiCloud9, DiDocker, DiHtml5, DiMysql, DiNodejs, DiPython, DiRasberryPi, DiReact, DiRuby, DiWordpress, DiJava } from "react-icons/di"
import { ExternalLink } from 'react-external-link';

function Techstack() {
  return (
    <Row style={{ justifyContent: "center", paddingBottom: "50px" }}>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://aws.amazon.com/fr/cloud9/"><DiCloud9 /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://www.docker.com/"><DiDocker /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://whatwg.org/"><DiHtml5 /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://www.oracle.com/java/"><DiJava /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://www.mysql.com/fr/"><DiMysql /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://nodejs.org/fr/"><DiNodejs /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://www.python.org/"><DiPython /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://www.raspberrypi.com/products/raspberry-pi-4-model-b/"><DiRasberryPi /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://fr.reactjs.org/"><DiReact /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://www.ruby-lang.org/fr/"><DiRuby /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://wordpress.com/fr/"><DiWordpress /></ExternalLink></Col>
    </Row>
  );
}

export default Techstack;
