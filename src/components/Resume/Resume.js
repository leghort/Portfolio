import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Particle from "../Particle";
import Resumecontent from "./ResumeContent";
import axios from "axios";
import pdf from "../../Assets/Cossu-mederic_cv.pdf";
import { AiOutlineDownload } from "react-icons/ai";

function Resume() {
  const uri = "https://porfolio-backend.vercel.app/ranks/getRanks";
  const [upadteSpojRank] = useState(0);
  const [upadteHackerank] = useState(0);
  const [upadateSem] = useState(0);
  const [upadteCgpa] = useState(0);

  useEffect(() => {
    axios
      .get(uri)
      .then((res) => {
        upadteSpojRank(res.data.message[0].spojRank);
        upadteHackerank(res.data.message[1].hackerrank);
        upadteCgpa(res.data.message[2].cgpa);
        upadateSem(res.data.message[3].sem);
      })
      .catch((err) => {
        console.log(err);
      });
  },);

  return (
    <Container fluid className="resume-section">
      <Particle />
      <Container>
        <Row style={{ justifyContent: "center", position: "relative" }}>
          <Button variant="primary" href={pdf} target="_blank">
            <AiOutlineDownload />
            &nbsp;Télécharger CV
          </Button>
        </Row>
        <Row className="resume">
          <Col md={6} className="resume-left">
            <h3 className="resume-title">Expériences</h3>
            <Resumecontent
              title="Technicien poste de travail [APHP]"
              date="Juin 2015 - Janvier 2018"
              content={[
                "Migration d'os",
                "Déploiement & installation de matériels informatique",
                "Maintenance de parc informatique (300 machines)",
                "Maintenance de serveurs",
                "Renouvellement du réseau",
                "Renouvellement du parc informatique",
                "Gestion des stocks et des commandes",
              ]}
            />
            <Resumecontent
              title="Électricien apprenti [BTP]"
              date="Séptembre 2011 - Juin 2013"
              content={[
                "Mise en place de contrôleur d'accès",
                "Tirage de câble d'installation éléctrique et incendie",
                "Création de l'installation électrique (maison, hotel, etc...)",
              ]}
            />
          </Col>
          <Col md={6} className="resume-right">
            <h3 className="resume-title">Formations</h3>
            <Resumecontent
              title="Bts SIO-SISR [Greta - Lognes]"
              date="Séptembre 2021 - En cour"
              content={[
                "Scripting shell",
                "Algorythm python 3",
              ]}
            />
            <Resumecontent
              title="Bac Pro SEN-TR [Greta - Louis Armand]"
              date="Octobre 2014 - Juillet 2015"
              content={[
                "Configuration IPBX & PABX",
                "Installation & configuration d'Active diectory",
                "Installation & configuration d'un DHCP/DNS",
                "Configuration de Switch & Router cisco",
              ]}
            />
          </Col>
        </Row>
        <Row style={{ justifyContent: "center", position: "relative" }}>
          <Button variant="primary" href={pdf} target="_blank">
            <AiOutlineDownload />
            &nbsp;Télécharger CV
          </Button>
        </Row>
      </Container>
    </Container>
  );
}

export default Resume;
