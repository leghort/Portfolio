import React from "react";
import { Col, Row } from "react-bootstrap";
import { SiLinux,SiVisualstudiocode,SiKeepassxc,SiPycharm,SiIntellijidea } from "react-icons/si";
import { DiGithubBadge,DiTrello,DiWindows } from "react-icons/di";
import { GiAtom } from "react-icons/gi";
import { ExternalLink } from 'react-external-link';

function Toolstack() {
  return (
    <Row style={{ justifyContent: "center", paddingBottom: "50px" }}>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://www.microsoft.com/fr-fr/windows"><DiWindows /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://www.debian.org/index.fr.html"><SiLinux /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://code.visualstudio.com/"><SiVisualstudiocode /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://desktop.github.com/"><DiGithubBadge /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://trello.com/"><DiTrello /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://atom.io/"><GiAtom /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://keepassxc.org/"><SiKeepassxc /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://www.jetbrains.com/fr-fr/pycharm/"><SiPycharm /></ExternalLink></Col>
      <Col xs={4} md={2} className="tech-icons"><ExternalLink href="https://www.jetbrains.com/fr-fr/idea/"><SiIntellijidea /></ExternalLink></Col>
    </Row>
  );
}

export default Toolstack;
