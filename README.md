<h2 align="center">
  Portfolio Website - v2.0<br/>
  <a href="http://cossu.xyz/" target="_blank">cossu.xyz</a>
</h2>

<br/>

<center>

[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com) &nbsp;
[![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)](https://forthebadge.com) &nbsp;
[![forthebadge](https://forthebadge.com/images/badges/open-source.svg)](https://forthebadge.com) &nbsp;
![GitHub Repo stars](https://img.shields.io/github/stars/leghort/Portfolio?color=red&logo=github&style=for-the-badge) &nbsp;
![GitHub forks](https://img.shields.io/github/forks/leghort/Portfolio?color=red&logo=github&style=for-the-badge)

</center>

<h3 align="center">
    🔹
    <a href="https://github.com/leghort/Portfolio/issues">Signaler un bug</a> &nbsp; &nbsp;
    🔹
    <a href="https://github.com/leghort/Portfolio/issues">Demander une fonctionnalité</a>
</h3>

## Build le projets
```bash
yarn ; yarn build ; [System.Diagnostics.Process]::Start("chrome.exe","--incognito http://localhost:3000/") ; yarn start
```
## Construit avec

Mon portfolio <a href="http://cossu.xyz/" target="_blank">cossu.xyz</a> certains de mes projets ainsi que mon CV et mes compétences techniques.<br/>

Ce projet a été construit en utilisant ces technologies.

- React.js
- Node.js
- Express.js
- CSS3
- VsCode
- Vercel

## Fonctions

**📖 Disposition multi-pages**

**🎨 Stylé avec React-Bootstrap et Css avec des couleurs faciles à personnaliser.**

**📱 Entièrement réactif "Responsive"**

## Pour commencer

Pour clonez ce dépôt. Vous aurez besoin de `node.js` et `git` d'installée sur votre machine.

## 🛠 Instructions d'installation

1. Installation: `npm install`

2. Dans le répertoire du projet, exécuter la commande: `npm start`

Lance l'application en mode développement.\
Ouvrir [http://localhost:3000](http://localhost:3000) pour l'afficher dans le navigateur.
La page se rechargera si vous apportez des modifications.

## Mode d'emploi

Ouvrez le dossier du projet et accédez à `/src/components/`. <br/>
Vous trouverez tous les composants utilisés et vous pourrez modifier vos informations en conséquence.

## Remerciement

Ce portfolio est un fork du projet de [Soumyajit4419](https://github.com/leghort/Portfolio). Merci à lui !
